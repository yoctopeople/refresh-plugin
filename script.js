function getRandomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}
const minRefreshTimeout = 1;
const maxRefreshTimeout = 90;
const seconds = getRandomInt(maxRefreshTimeout - minRefreshTimeout) + minRefreshTimeout;
console.log(`Refresh in ${seconds} seconds`);
setTimeout(()=> location.reload(true), seconds * 1000);